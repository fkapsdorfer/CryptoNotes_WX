/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file Background script for WebExtension. Provides functionality of browser action button, when clicked opens the app.
 */

var url    = chrome.extension.getURL("app/index.html");
var winOpen= false;

chrome.browserAction.onClicked.addListener(() => {

    // Do not open if already open
    if (winOpen) return;

    chrome.windows.create({
        url: url,
        type: "panel",
        height: 480,
        width: 360
    }, function (win) {
        winOpen = true;

        chrome.windows.onRemoved.addListener(() => {
            winOpen = false;
        });
    });

});
