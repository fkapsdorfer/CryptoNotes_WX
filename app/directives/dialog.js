/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file Textural dialog.
 */

angular.module('CryptoNotes').directive('dialog', function () {

    return {
        replace: true,
        scope: {
            title    : '=?',
            msg      : '=?',
            // ctrl: {
            //   onOpen(), onClose(),   // defined outside
            //   open(), hide(), shown  // defined by directive
            // }
            ctrl     : '=',
            // If should open right away
            show     : '&?'
        },
        templateUrl: '/static/app/directives/dialog.html',

        link: function (scope, $dialog) {

            var $doc = $(document);

            if (scope.show) {
                openDialog();
            }


            // Input controls
            $dialog.on('click', evt => {
                if (evt.target.classList.contains('dialog')) {
                    closeDialog();
                }
            });

            $dialog.find('.icon-cross')
            .on('click', () => {
                closeDialog();
            })
            .on('keypress', evt => {
                // return key
                if (evt.keyCode === 13) {
                    closeDialog();
                }
            });

            var escHandler = evt => {
                // escape key
                if (evt.keyCode === 27) {
                    closeDialog();
                }
            };


            function closeDialog () {
                var handler = evt => {
                    // .dialog should end transition the last
                    // transitionend will also bubble from .container
                    if (evt.target.classList.contains('dialog')) {
                        $dialog.off('transitionend', handler);
                        $dialog.css('display', 'none');

                        scope.ctrl.shown = false;

                        var onClose = scope.ctrl.onClose
                        if (typeof onClose === 'function') {
                            onClose();
                        }
                    }
                };

                $dialog.on('transitionend', handler)
                $dialog.removeClass('shown');
                $doc.off('keyup', escHandler);
            }

            function openDialog () {
                var handler = evt => {
                    if (evt.target.classList.contains('container')) {
                        $dialog.off('transitionend', handler);

                        scope.ctrl.shown = true;

                        var onOpen = scope.ctrl.onOpen
                        if (typeof onOpen === 'function') {
                            onOpen();
                        }
                    }
                };

                $dialog.on('transitionend', handler);
                $dialog.css('display', 'block');
                $dialog.addClass('shown');
                $doc.on('keyup', escHandler);
            }

            scope.ctrl.open = openDialog;
            scope.ctrl.close = closeDialog;
        }
    }

});
