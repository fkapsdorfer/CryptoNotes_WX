/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file Acts as a import button. Communicates with SecureRecordCollection.
 */

angular.module('CryptoNotes').directive('srImport', ['SecureRecord', 'SecureRecordCollection', function (SecureRecord, src) {

    return {
        replace: false,
        scope: {
            srImport: '=' // Control object: onError, onJson, maxSize
        },

        link: function (scope, $el) {

            var ctrl = scope.srImport;
            var $doc = $(document);

            var $input  = $('<input type="file" hidden>');
            $input.css({
                visibility : 'hidden',
                position   : 'absolute',
                height     : 0,
                width      : 0,
                'font-size': 0
            });
            $input.appendTo($el);
            $input.on('change', onChange);
            $input.on('click', evt => evt.stopPropagation());

            $el.on('click keyup', evt => {
                // If not click and not return key
                if (evt.type !== 'click' && evt.keyCode !== 13) return;

                $input.click()
            });


            function onChange () {
                var file    = $input[0].files[0];

                if (!file) return; // Cancel chosen from dialog (enter invalid file and then chose cancel)

                var maxSize = ctrl.maxSize || 1024*1024;
                var reader 	= new FileReader();
                var name    = file.name;
                var data;

                // Check size
                if (maxSize < file.size) {
                    return ctrl.onError(new Error('SizeExceeded'));
                }

                $(reader).one('load', (evt) => {
                    try {
                        data = JSON.parse(evt.target.result);

                        if (
                            typeof data.name !== 'string' ||
                            typeof data.data !== 'string' ||
                            typeof data.key !== 'string' ||
                            typeof data.iv !== 'string'
                        ) {
                            return ctrl.onError(new Error('MissingFields'));
                        }

                        delete data._id;

                        var sr = new SecureRecord(data);
                        src.add(sr).then(ctrl.onImported);

                    } catch (err) {
                        return ctrl.onError(new Error('ParseError'));
                    }
                });

                // Read
                reader.readAsText(file);
            }

        }
    }

}]);
