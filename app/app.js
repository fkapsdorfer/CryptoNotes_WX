
angular.module('CryptoNotes', [
    'ui.router'
]).config([ '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise("/list");

    $stateProvider.state('List', {
        url: '/list',
        controller: 'List',
        templateUrl: './templates/list.html'
    }).state('Unlock', {
        url: '/unlock',
        controller: 'Unlock',
        templateUrl: './templates/unlock.html',
        params: { sr: null }
    }).state('Detail', {
        url: '/detail',
        controller: 'Detail',
        templateUrl: './templates/detail.html',
        params: { sr: null, data: null }
    }).state('Lock', {
        url: '/lock',
        controller: 'Lock',
        templateUrl: './templates/lock.html',
        params: { sr: null, data: null }
    });
}]);
