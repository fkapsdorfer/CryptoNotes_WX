/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file SecureRecord model.
 */

angular.module('CryptoNotes').factory('SecureRecord', ['CryptoLib', function (Crypto) {

    /**
     * Bctypts r.plainKey, and encrypts r.plainData if given.
     * Sets other properties.
     *
     * @param {Object}   r              Two alternatives - alt1 (from db), alt2 (new)
     * @param {String}   [r._id]        alt1
     * @param {String}   r.name         alt1, alt2
     * @param {String}   r.key          alt1
     * @param {String}   r.iv           alt1
     * @param {String}   [r.data]       alt1
     * @param {String}   r.plainKey     alt2
     * @param {String}   [r.plainData]  alt2
     * @param {Function} [cb]           Optional callback if r.plainKey / r.plainData is given
     * @returns SecureRecord
     */
    var SecureRecord = function (r, cb) {
        var u = undefined;
        var t = this;
        if (r.name != u && r.key != u && r.iv != u) {
            t.name = r.name;
            t.key  = r.key;
            t.iv   = r.iv;
            t.data = r.data;
            t._id  = r._id;
        } else {
            Promise.all([
                Crypto.bcrypt(r.plainKey),
                new Promise((resolve) => {
                    resolve(r.plainData
                        ? Crypto.encrypt(r.plainKey, r.plainData)
                        : null
                    )
                })
            ]).then((res) => {
                this.name = r.name;
                this.key  = res[0];

                // if r.plainData has been set
                if (res[1]) {
                    this.iv   = res[1].iv;
                    this.data = res[1].data;
                }

                // callback
                if (typeof cb === 'function') cb();
            });
        }
    }

    /**
     * Sets data and iv.
     * Rejects the promise if the (SecureRecord) key is not set.
     * Rejects the promise if the given plainKey
     * does not match with the stored (bcrypt) key.
     *
     * @param {String} plainKey
     * @param {String} data
     * @returns Promise
     */
    SecureRecord.prototype.setData = function (plainKey, data) {

        if (!this.key) {
            return Promise.reject(new Error('Key is not set'));
        }

        return this.isKeyMatching(plainKey).then((res) => {
            if (!res) {
                return Promise.reject(new Error('Key is not matching'));
            }
            return Crypto.encrypt(plainKey, data);
        }).then((res) => {
            this.iv   = res.iv;
            this.data = res.data;
        });
    }

    /**
     * Rejects the promise if the (SecureRecord) key or iv is not set.
     * Rejects the promise if the given plainKey
     * does not match with the stored (bcrypt) key.
     *
     * @param {String} plainKey
     * @returns Promise<String>
     */
    SecureRecord.prototype.getData = function (plainKey) {
        return new Promise((resolve, reject) => {
            if (!this.iv) {
                reject(new Error('IV is not set'));
            } else if (!this.key) {
                reject(new Error('Key is not set'));
            } else {
                this.isKeyMatching(plainKey).then((res) => {
                    if (!res) {
                        reject(new Error('Bad Key'));
                    } else {
                        resolve(Crypto.decrypt(plainKey, this.iv, this.data));
                    }
                });
            }
        });
    }

    /**
     * @param {String} plainKey
     * @returns Promise<Boolean>
     */
    SecureRecord.prototype.isKeyMatching = function (plainKey) {
        return Crypto.bcryptCompare(plainKey, this.key);
    }

    /**
     * @returns Object
     */
    SecureRecord.prototype.export = function () {
        var t = this;

        var o = {
            name: t.name,
            key : t.key,
            iv  : t.iv,
            data: t.data,
        };

        if (t._id != undefined) {
            o._id = t._id;
        }

        return o;
    }

    return SecureRecord;

}]);
