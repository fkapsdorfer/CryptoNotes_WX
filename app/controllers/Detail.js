/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file The Detial state controller.
 */

angular.module('CryptoNotes').controller('Detail', ['$scope', '$state', '$stateParams', 'SecureRecord', 'SecureRecordCollection', function($scope, $state, $stateParams, SecureRecord, src){
    var sr = $scope.sr = $stateParams.sr;
    var data = $scope.data = $stateParams.data;

    if (!sr || data == undefined) {
        $state.go('List');
    }
}]);
