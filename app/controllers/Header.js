/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file The Header controller.
 */

angular.module('CryptoNotes').controller('Header', ['$scope', function($scope){

    $scope.importCtrl = {
        maxSize: 1024,
        onJson: (data) => {
            $scope.$root.$broadcast('dialog', {
                title: 'Success!',
                msg  : 'Successfully imported the new entry.'
            });
        },
        onError: (err) => {
            switch (err.message) {
                case 'SizeExceeded':
                    var limit = ($scope.importCtrl.maxSize / 1024).toFixed(2);
                    $scope.$root.$broadcast('dialog', {
                        title: 'File Size Exceeded',
                        msg  : `The file is too large. Limit ${limit}KB `
                    });
                    break;
                case 'ParseError':
                    $scope.$root.$broadcast('dialog', {
                        title: 'Invalid Format',
                        msg  : 'Given JSON data could not be parsed.'
                    });
                    break;
                case 'MissingFields':
                    return scope.$root.$broadcast('dialog', {
                        title: 'Missing Field(s)',
                        msg  : 'Some fields are missing.'
                    });
                    break;
                default:
                    throw err;
            }
        }
    };

}]);
