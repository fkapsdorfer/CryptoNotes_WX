

CryptoNotes_WX
==============
A browser extension for encrypting notes.


Functionality
-------------

* **Encrypt** notes using `AES-256-CBC` with custom **passphrase** `(bcrypt (60b) -> SHA-256)`, and **auto-generated** `IV (128b)`
* **List** records by name in the `List` view with **actions**
* **Remove** records
* **Decrypt** notes and **view** the content in the `Detail` view
* The individual records can be **exported** as `JSON` file
* `JSON` file can be used to **import** a record


Installation
------------
1. Clone the repository.
2. Load unpacked extension in a modern browser that supports [WebExtensions](https://developer.mozilla.org/en-US/Add-ons/WebExtensions) (e.g. Chrome, Firefox)


Running The App
---------------
After you loaded the extension a button will be added aside the address bar. Once you click it the application will open.

#### Persistence
Note that `nedb` library used for persistence chooses the underlying storage technology (it could be IndexDB, WebSQL, LocalStorage).


To Be Implemented
--------------------
* Loading Screen & UI Locking.
